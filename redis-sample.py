import redis

facts = {'fname': 'Sergey', 'lname': "Marfenko"}
facts['age'] = 31
facts.update({'fname': 'Petr'})
facts.pop('lname')

print(facts)

r = redis.Redis(host='192.168.101.210', port='8080')

#key = 'redisKey'
#value = 'redisValue'

#r.set(key, value)

#print(r.get(key).decode("UTF-8"))

for key, value in facts.items():
    print('key: ' + key + ' value: ' + str(value))
    r.set(key , value)



