## Занятие по Redis

[Задания для лаб](https://gitlab.com/Marfich/redis-lecture/-/blob/master/labs.txt)\
[Презентация](https://gitlab.com/Marfich/redis-lecture/-/blob/master/Key-value_storage.pdf)\
[Пример Python кода с лекции](https://gitlab.com/Marfich/redis-lecture/-/blob/master/redis-sample.py)\
[Лог команд с лекции](https://gitlab.com/Marfich/redis-lecture/-/blob/master/commands.log)

#### Полезные ссылки:
1) [доки по Python dict](https://python-reference.readthedocs.io/en/latest/docs/dict/)
2) [доки по Java HashMap](https://docs.oracle.com/javase/8/docs/api/java/util/HashMap.html)
3) [Redis](https://redis.io/documentation)
4) [модуль Redis для Python 3](https://pypi.org/project/redis/)
5) [Docker Hub - Redis](https://hub.docker.com/_/redis?tab=description)
